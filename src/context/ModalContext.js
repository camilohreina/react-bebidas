import React, { useState, createContext, useEffect } from "react";
import axios from "axios";

// Crear el context

export const ModalContext = createContext();

const ModalProvider = (props) => {
  // State del provider
  const [idreceta, setIdreceta] = useState(null);
  const [infoReceta, setReceta] = useState({});

  // Una vez que tenemos una receta, llamamos a la api
  useEffect(() => {
    const obtenerReceta = async () => {
      if (!idreceta) return;
      const url = `https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${idreceta}`;
      const resultado = await axios.get(url);
      setReceta(resultado.data.drinks[0]);
    };
    obtenerReceta();
  }, [idreceta]);

  return (
    <ModalContext.Provider value={{ infoReceta, setIdreceta, setReceta }}>
      {props.children}
    </ModalContext.Provider>
  );
};

export default ModalProvider;
